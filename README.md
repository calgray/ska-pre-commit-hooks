# ska-pre-commit-hooks

SKA pre-commit hooks for git developer workflows.

[pre-commit](https://pre-commit.com/) is a configurable Python tool useful for identifying simple issues when committing changes and before submission to code review. This repository provides a central location for pre-commit hook definitions for developers to choose from in ska Python projects.

**pre-commit** works for all available git hooks (not just limited to pre-commit):
* commit-msg
* post-checkout
* post-commit
* post-merge
* post-rewrite
* pre-commit
* pre-merge-commit
* pre-push
* pre-rebase
* prepare-commit-msg

## Repository Setup Instructions

In a seperate checked-out Python repository using poetry:

1. add the following to a `.pre-commit-config.yaml` file in the root project directory:

```yaml
repos:
- repo: https://gitlab.com/calgray/ska-pre-commit-hooks
  rev: 89d32784e5bdb24dc151939c833eb285567ef2ea
  hooks:
  # python lint
  - id: isort
  - id: black
  - id: flake8
  - id: pylint
  # jira ticket checks
  - id: branch ticket id
  - id: commit msg ticket id
```

2. Install or add `pre-commit` as a package dependency:

```bash
poetry add pre-commit
poetry install
```

3. Test selected pre-commit passes for all files:

```bash
pre-commit run --all-files
```

4. Commit and submit the merge request for developers to opt into git hooks.

## Opt-in to pre-commit hooks in developement

Install hooks on developer environments wishing to use pre-commit hooks:

```bash
pre-commit install -t pre-commit
pre-commit install -t commit-msg
```

`git commit` will now automatically run hooks for staged changes and commit messages e.g.

```sh
>>> git commit -m "ABC-123 commit message"
poetry isort.............................................................Passed
poetry black.............................................................Passed
poetry flake8............................................................Passed
poetry pylint............................................................Passed
branch ticket ID.........................................................Passed
commit message ticket ID.................................................Passed
[abc-123 88dfe4c] ABC-123 commit message
 1 file changed, 1 insertion(+), 1 deletion(-)
```

For a detailed description of pre-commit use cases see https://pre-commit.com

## Opt-out of pre-commit hooks in developement

Commit hooks can be explicity skipped by using the `--no-verify` option. e.g.

```sh
>>> git commit -m "unchecked commit" --no-verify
[abc-123 88dfe4c] unchecked commit
 1 file changed, 1 insertion(+), 1 deletion(-)
```

To opt-out completely from git hook integration, pre-commit hooks may also be uninstalled:

```bash
pre-commit uninstall -t pre-commit
pre-commit uninstall -t commit-msg
```

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-pre-commit-hooks/badge/?version=latest)](https://developer.skao.int/projects/ska-pre-commit-hooks/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-pre-commit-hooks documentation](https://developer.skatelescope.org/projects/ska-pre-commit-hooks/en/latest/index.html "SKA Developer Portal: ska-pre-commit-hooks documentation")

